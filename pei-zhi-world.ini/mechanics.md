# Mechanics

### 8 - MECHANICS <a href="#id-8-mechanics" id="id-8-mechanics"></a>

指定某些游戏机制的设置。

#### 可选选项 <a href="#ke-xuan-xuan-xiang" id="ke-xuan-xuan-xiang"></a>

| 变量                    | 描述                                                  | 缺省值 |
| --------------------- | --------------------------------------------------- | --- |
| CommandBlocksEnabled  | <p>启用/禁用命令方块。<br>命令方块在当前阶段非常实验性的。<br>布尔值：可为0或1。</p> | 0   |
| PVPEnabled            | <p>启用/禁用PVP。<br>布尔值：可为0或1。</p>                      | 1   |
| UseChatPrefixes       | <p>启用/禁用所有玩家的聊天前缀。<br>布尔值：可为0或1。</p>                | 1   |
| MinNetherPortalWidth  | 设置下界传送门的最小宽度。                                       | 2   |
| MaxNetherPortalWidth  | 设置下界传送门的最大宽度。                                       | 21  |
| MinNetherPortalHeight | 设置下界传送门的最小高度。                                       | 3   |
| MaxNetherPortalHeight | 设置下界传送门的最大高度。                                       | 21  |

\
