# Monsters

### 9 - MONSTERS <a href="#id-9-monsters" id="id-9-monsters"></a>

指定怪物的设置。

#### 可选选项 <a href="#ke-xuan-xuan-xiang" id="ke-xuan-xuan-xiang"></a>

| 变量                          | 描述                                                | 缺省值                                                                                                                                                      |
| --------------------------- | ------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------- |
| VillagersShouldHarvestCrops | <p>村民目前还干不了什么，<br>因此此设置不会产生任何变化。</p>              | 1                                                                                                                                                        |
| AnimalsOn                   |                                                   | 1                                                                                                                                                        |
| Types                       | <p>设置允许的怪物列表（包括攻击性和被动攻击）。<br>所有怪物应为小写，并用逗号分隔。</p> | bat, cavespider, chicken, cow, creeper, guardian, horse, mooshroom, ocelot, pig, rabbit, sheep, silverfish, skeleton, slime, spider, squid, wolf, zombie |
