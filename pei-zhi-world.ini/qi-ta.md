# 其他

### [12 - 其他](https://wiki.cuberite.dfggmc.top/#/3\_12\_Other?id=\_12-%e5%85%b6%e4%bb%96) <a href="#id-12-qi-ta" id="id-12-qi-ta"></a>

#### [可选选项](https://wiki.cuberite.dfggmc.top/#/3\_12\_Other?id=%e5%8f%af%e9%80%89%e9%80%89%e9%a1%b9) <a href="#ke-xuan-xuan-xiang" id="ke-xuan-xuan-xiang"></a>

| 变量              | 描述                           | 缺省值 |
| --------------- | ---------------------------- | --- |
| ProtectRadius   | 设置非管理员玩家无法建造的半径范围。           | 10  |
| WorldDifficulty | 设置此世界的难度级别。                  | 0   |
| LimitRadius     | 限制世界的范围，类似于Nothcian 香草端世界边界。 | 1   |
