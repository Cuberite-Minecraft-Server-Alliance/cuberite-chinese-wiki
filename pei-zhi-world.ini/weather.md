# Weather

### [10 - WEATHER](https://wiki.cuberite.dfggmc.top/#/3\_10\_Weather?id=\_10-weather) <a href="#id-10-weather" id="id-10-weather"></a>

指定世界上的天气设置。所有的数值均以Tick为单位。

#### [可选选项](https://wiki.cuberite.dfggmc.top/#/3\_10\_Weather?id=%e5%8f%af%e9%80%89%e9%80%89%e9%a1%b9) <a href="#ke-xuan-xuan-xiang" id="ke-xuan-xuan-xiang"></a>

| 变量                   | 描述 | 缺省值    |
| -------------------- | -- | ------ |
| MaxSunnyTicks        |    | 180000 |
| MinSunnyTicks        |    | 12000  |
| MaxRainTicks         |    | 24000  |
| MinRainTicks         |    | 12000  |
| MaxThunderStormTicks |    | 15600  |
| MinThunderStormTicks |    | 3600   |
