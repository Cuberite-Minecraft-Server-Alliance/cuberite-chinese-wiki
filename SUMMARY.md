# Table of contents

## 介绍 <a href="#introduction" id="introduction"></a>

* [介绍](README.md)
* [什么是Cuberite](introduction/shi-mo-shi-cuberite.md)
* [历史](introduction/li-shi.md)

## 安装 <a href="#install" id="install"></a>

* [预编译版本](install/yu-bian-yi-ban-ben.md)
* [自行编译 Cuberite](install/zi-hang-bian-yi-cuberite.md)
* [启动Cuberite](install/qi-dong-cuberite.md)

## 基础配置

* [配置概述](ji-chu-pei-zhi/pei-zhi-gai-shu.md)
* [权限](ji-chu-pei-zhi/quan-xian.md)
* [WebAdmin](ji-chu-pei-zhi/webadmin.md)
* [世界](ji-chu-pei-zhi/shi-jie.md)
* [插件](ji-chu-pei-zhi/cha-jian.md)

## 配置World.ini

* [什么是 world.ini](pei-zhi-world.ini/shi-mo-shi-world.ini.md)
* [General](pei-zhi-world.ini/general.md)
* [Broadcasting](pei-zhi-world.ini/broadcasting.md)
* [SpawnPosition](pei-zhi-world.ini/spawnposition.md)
* [Storage](pei-zhi-world.ini/storage.md)
* [Physics](pei-zhi-world.ini/physics.md)
* [Mechanics](pei-zhi-world.ini/mechanics.md)
* [Monsters](pei-zhi-world.ini/monsters.md)
* [Weather](pei-zhi-world.ini/weather.md)
* [Generator](pei-zhi-world.ini/generator.md)
* [其他](pei-zhi-world.ini/qi-ta.md)
* [Example Configurations](pei-zhi-world.ini/example-configurations.md)

## 多世界 <a href="#multiworlds" id="multiworlds"></a>

* [多世界概述](multiworlds/duo-shi-jie-gai-shu.md)
* [使用命令进行穿越](multiworlds/shi-yong-ming-ling-jin-hang-chuan-yue.md)
* [在没有插件的情况下连接世界](multiworlds/zai-mei-you-cha-jian-de-qing-kuang-xia-lian-jie-shi-jie.md)
* [使用插件连接世界](multiworlds/shi-yong-cha-jian-lian-jie-shi-jie.md)
* [BungeeCord](multiworlds/bungeecord.md)

## 跨版本与基岩版支持

* [原生跨版本](kua-ban-ben-yu-ji-yan-ban-zhi-chi/yuan-sheng-kua-ban-ben.md)
* [使用ViaProxy进行跨版本](kua-ban-ben-yu-ji-yan-ban-zhi-chi/shi-yong-viaproxy-jin-hang-kua-ban-ben.md)
* [使用Geyser获得基岩版支持](kua-ban-ben-yu-ji-yan-ban-zhi-chi/shi-yong-geyser-huo-de-ji-yan-ban-zhi-chi.md)

## Cuberite API - 文章

* [编写Cuberite插件](cuberite-api-wen-zhang/bian-xie-cuberite-cha-jian.md)
* [使用Info.lua文件](cuberite-api-wen-zhang/shi-yong-info.lua-wen-jian.md)
* [设置 Decoda IDE](cuberite-api-wen-zhang/she-zhi-decoda-ide.md)
* [设置 ZeroBrane Studio IDE](cuberite-api-wen-zhang/she-zhi-zerobrane-studio-ide.md)
