# 使用命令进行穿越

### [2 - 使用命令进行穿越](https://wiki.cuberite.dfggmc.top/#/4\_2\_Traveling-by-Command?id=\_2-%e4%bd%bf%e7%94%a8%e5%91%bd%e4%bb%a4%e8%bf%9b%e8%a1%8c%e7%a9%bf%e8%b6%8a) <a href="#id-2-shi-yong-ming-ling-jin-hang-chuan-yue" id="id-2-shi-yong-ming-ling-jin-hang-chuan-yue"></a>

如果你有`core.portal`权限，可以使用`/world <Worldname>`命令在不同世界之间进行穿越。要列出所有可用的世界，请在拥有`core.portal`权限的前提下使用`/worlds`命令来获取世界列表。
