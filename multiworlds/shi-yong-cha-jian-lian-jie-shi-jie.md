# 使用插件连接世界

### [4 - 使用插件连接世界](https://wiki.cuberite.dfggmc.top/#/4\_4\_Linking-Worlds-with-a-Plugin?id=\_4-%e4%bd%bf%e7%94%a8%e6%8f%92%e4%bb%b6%e8%bf%9e%e6%8e%a5%e4%b8%96%e7%95%8c) <a href="#id-4-shi-yong-cha-jian-lian-jie-shi-jie" id="id-4-shi-yong-cha-jian-lian-jie-shi-jie"></a>

将世界连接在一起的最可配置的方法是使用专用插件，比如[Portal V2](https://forum.cuberite.org/thread-2157.html)。你可以在[插件库](https://forum.cuberite.org/forum-2.html)中找到更多的插件。
