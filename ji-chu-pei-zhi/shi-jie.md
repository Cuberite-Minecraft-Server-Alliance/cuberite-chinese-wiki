# 世界

## 4 - 世界 <a href="#id-4-shi-jie" id="id-4-shi-jie"></a>

默认情况下，有三个世界：`world`、`world_nether`、`world_the_end`。每个世界都可以通过编辑各个世界文件夹中的`world.ini`进行调整。您可以使用该文件来编辑诸如生成点位置、游戏模式和难度级别等内容。有关详细信息，请参阅[配置world.ini](https://wiki.cuberite.dfggmc.top/#/document/MDD\_Cuberite\_Users\_Manual/3\_CONFIGURING\_WORLD\_INI/3\_1\_What\_is\_world-ini)。
